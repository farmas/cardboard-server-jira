/// <reference path="typings/cardboard/cardboard.d.ts" />

$(function () {
    var $ = jQuery,
        server = (<any>window).server,
        boardColumns: BoardColumn[] = [
            {
                name: "",
                width: "10%"
            },
            {
                name: "Open",
                width: "40%",
                showCellCount: true
            },
            {
                name: "In Progress",
                width: "20%"
            },
            {
                name: "Done",
                width: "20%"
            }
        ],
        shellGroupByList: BoardRowsGroupBy[] = [
            {
                name: "Owner",
                cardProperty: "owner"
            }];

    $("#board").cardboard({
        getCardsAsync: (board: BoardClientSettings, query: string) => {
            return $.ajax(server.apiPath + "/Cards?" + query, {
                dataType: "json"
            });
        },
        getCardUrl: (card: BoardCardData): string => {
            return "https://issues.jenkins-ci.org/browse/JENKINS-" + card.id;
        },
        startupBoardId: "bugs",
        boards: [
            {
                id: "bugs",
                name: "Bugs",
                query: {
                    issueType: "Bug"
                },
                columns: boardColumns,
                groupByList: shellGroupByList
            },
            {
                id: "improvements",
                name: "Improvements",
                query: {
                    issueType: "Improvement"
                },
                columns: boardColumns,
                groupByList: shellGroupByList
            },
            {
                id: "tasks",
                name: "Tasks",
                query: {
                    issueType: "Task"
                },
                columns: boardColumns,
                groupByList: shellGroupByList
            },
            {
                id: "newFeatures",
                name: "New Features",
                query: {
                    issueType: "New Feature"
                },
                columns: boardColumns,
                groupByList: shellGroupByList
            }]
    });
});