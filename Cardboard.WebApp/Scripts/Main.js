/// <reference path="typings/cardboard/cardboard.d.ts" />
$(function () {
    var $ = jQuery, server = window.server, boardColumns = [
        {
            name: "",
            width: "10%"
        },
        {
            name: "Open",
            width: "40%",
            showCellCount: true
        },
        {
            name: "In Progress",
            width: "20%"
        },
        {
            name: "Done",
            width: "20%"
        }
    ], shellGroupByList = [
        {
            name: "Owner",
            cardProperty: "owner"
        }
    ];
    $("#board").cardboard({
        getCardsAsync: function (board, query) {
            return $.ajax(server.apiPath + "/Cards?" + query, {
                dataType: "json"
            });
        },
        getCardUrl: function (card) {
            return "https://issues.jenkins-ci.org/browse/JENKINS-" + card.id;
        },
        startupBoardId: "bugs",
        boards: [
            {
                id: "bugs",
                name: "Bugs",
                query: {
                    issueType: "Bug"
                },
                columns: boardColumns,
                groupByList: shellGroupByList
            },
            {
                id: "improvements",
                name: "Improvements",
                query: {
                    issueType: "Improvement"
                },
                columns: boardColumns,
                groupByList: shellGroupByList
            },
            {
                id: "tasks",
                name: "Tasks",
                query: {
                    issueType: "Task"
                },
                columns: boardColumns,
                groupByList: shellGroupByList
            },
            {
                id: "newFeatures",
                name: "New Features",
                query: {
                    issueType: "New Feature"
                },
                columns: boardColumns,
                groupByList: shellGroupByList
            }
        ]
    });
});
