﻿using Atlassian.Jira;
using Cardboard.Server;
using Cardboard.Server.Models;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace Cardboard.WebApp
{
    public class JiraCardService : ICardService
    {
        private static Jira _jira = Jira.CreateRestClient("https://issues.jenkins-ci.org", null, null);

        public IEnumerable<Card> GetCards(HttpRequestMessage request, string boardId)
        {
            _jira.MaxIssuesPerRequest = 50;

            var queryString = request.GetQueryNameValuePairs().ToDictionary(q => q.Key, q => q.Value, StringComparer.OrdinalIgnoreCase);
            var issueType = queryString["issueType"];

            var issues = (from issue in _jira.Issues
                          where issue.Created >= DateTime.Now.AddYears(-1)
                          where issue.Project == "JENKINS"
                          where issue.Type == issueType
                          select issue).ToArray();

            return issues.Select(issue => new Card()
                   {
                       Id = issue.Key.Value.Substring(8),
                       Title = issue.Summary,
                       Owner = issue.Assignee,
                       Priority = GetCardPriority(issue),
                       Status = GetCardStatus(issue)
                   });
        }

        private int GetCardPriority(Issue issue)
        {
            switch (issue.Priority.Name.ToLowerInvariant())
            {
                case "blocker":
                case "critical":
                    return 0;
                case "major":
                    return 1;
                default:
                    return 2;
            }
        }

        private string GetCardStatus(Issue issue)
        {
            var issueStatus = issue.Status.Name;

            if ("Resolved".Equals(issueStatus, StringComparison.OrdinalIgnoreCase)
                || "Closed".Equals(issueStatus, StringComparison.OrdinalIgnoreCase))
            {
                return "Done";
            }

            return issueStatus;
        }
    }
}